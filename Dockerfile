FROM 589486629198.dkr.ecr.eu-west-1.amazonaws.com/sp-docker-miniconda:latest

COPY dpd_upload.ipynb .

CMD ["./start.sh", "dpd_upload.ipynb", "dpd_upload_output.ipynb"]
