##Requirements

Package | Version
:---: | :---:
`awscli` | `>v1.17.10`

## Running image locally

### Local papermill run
To run the papermill locally the way it would run on the scheduled task use this command:

```
docker-compose up --build
```

### Local jupyter server run

You can also run the local jupyter server by running this command:

```
JUPYTER_RUN=true docker-compose up --build
```

this will start a jupyter server that you can edit the file in and test it. When the job runs it's going to show you:

````
app_1                  |     To access the notebook, open this file in a browser:
app_1                  |         file:///root/.local/share/jupyter/runtime/nbserver-7-open.html
app_1                  |     Or copy and paste one of these URLs:
app_1                  |         http://5a015b763d6a:8888/?token=f84a7518862bd7cddb510325c7fad09dbc9c231afead27e8
app_1                  |      or http://127.0.0.1:8888/?token=f84a7518862bd7cddb510325c7fad09dbc9c231afead27e8
````

go to one of the urls in the list and 